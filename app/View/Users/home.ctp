<!doctype html>

<html lang="en-US">
<head>
<meta charset="UTF-8" />
<title>Free CSS Template : Small Business</title>
<link href="style.css" rel="stylesheet" type="text/css">
</head>
<body>
<div id="wrap">

<nav id="mainnav">

<h1 id="textlogo">
Eagle<span> Financial</span>
</h1>
<ul>

	<li><a href="#">Home</a></li>
    <li><a href="login">Login</a></li>
	</ul>
</nav>

<section id="content">
<header id="homeheader">
</header>
<footer>
<div id="bottom">
<a href="Customers">Customers</a> | <a href="Investments">Investments</a> |<a href="stocks">Stocks</a>
</div>
<div id="credits">
2012 &copy; All Rights Reserved.
</div>
</footer>
</body>
</html>
