<!DOCTYPE HTML>
<!-- Website template by freewebsitetemplates.com -->
<html>
<head>
	<meta charset="UTF-8">
	<title>Law Firm</title>
	<link rel="stylesheet" href="css/style.css" type="text/css">
</head>
<body>
	<div id="header">
		<div class="clearfix">
			<div class="logo">
				<a href="#"><img src="images/logo.png" alt="LOGO" height="52" width="362"></a>
			</div>
			<ul class="navigation">
				<li class="active">
					<a href="#">Home</a>
				</li>
				<li>
					<a href="logout">Logout</a>
				</li>
			</ul>
		</div>
	</div>
	
		<div class="featured">
			<h2>Why Choose Us?</h2>
			<ul class="clearfix">
				<li>
					<div class="frame1">
						<div class="box">
							<img src="images/b.jpg" alt="Img" height="130" width="197">
						</div>
					</div>
					<p>
						<b>Customer</b> </p>
					<a href="customers" class="more">visit</a>
				</li>
				<li>
					<div class="frame1">
						<div class="box">
							<img src="images/a.jpg" alt="Img" height="130" width="197">
						</div>
					</div>
					<p><b>Investments</b>
				 </p>
					<a href="investments" class="more">visit</a>
				</li>
				<li>
					<div class="frame1">
						<div class="box">
							<img src="images/d.jpg" alt="Img" height="130" width="197">
						</div>
					</div>
					<p>
						<b>Stocks</b> 	</p>
					<a href="stocks" class="more">Visit</a>
				</li>
				<li>
					<div class="frame1">
						<div class="box">
							<img src="images/c.png" alt="Img" height="130" width="197">
						</div>
					</div>
					<p>
						<b>Generate report</b> </p>
					<a href="report_manager/reports" class="more">visit</a>
				</li>
			</ul>
		</div>
	</div>
			<div class="section contact">
				<h4>Contact Us</h4>
				<p>
					<span>Address:</span> the address city, state 1111
				</p>
				<p>
					<span>Phone:</span> (+20) 000 222 999
				</p>
				<p>
					<span>Email:</span> info@Eagle.com
				</p>
			</div>
		
				<p>
					© Copyright 2016 nanumula. All Rights Reserved.
				</p>
			</div>
		</div>
	</div>
</body>
</html>